package com.adistec.mailclient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface IMailClient {
	ResponseEntity<?> sendFeedback(String message, String appDesc, String appName, String userEmail, String userCompany, String userName, String subject, String application) throws JsonProcessingException, URISyntaxException;
	ResponseEntity<?>sendRecovery(String userEmail, String hash, String appUrl, String subject, String application) throws JsonProcessingException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables, String subject, Map<String, InputStream> images, String application) throws URISyntaxException, IOException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables, String subject, Map<String, InputStream> images, Map<String, InputStream> attachments, String[] copyTo, String application) throws URISyntaxException, IOException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables, String subject, Map<String, InputStream> images, String from, String application) throws URISyntaxException, IOException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables, String subject, Map<String, InputStream> images, String from, String[] copyTo, String application) throws URISyntaxException, IOException;
	ResponseEntity<?> sendEmailWithContent(String[] sentToEmail,String content , String subject, Map<String, InputStream> images, String from, String[] copyTo,String[] cco, String application) throws URISyntaxException, IOException;
	ResponseEntity<?> getMailNotifications(String email, String application);
}
