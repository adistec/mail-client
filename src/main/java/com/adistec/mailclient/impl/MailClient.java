package com.adistec.mailclient.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.adistec.mailclient.IMailClient;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.util.UriComponentsBuilder;

public class MailClient implements IMailClient {
	private String mailSenderUrl;
	private HttpHeaders headers;
	/**
	 * 
	 * @param mailSenderUrl format: http://x.x.x.x:port/
	 */
	public MailClient(String mailSenderUrl) {
		this.initialize(mailSenderUrl, null);
	}
	
	/**
	 * 
	 * @param mailSenderUrl format: http://x.x.x.x:port/
	 * @param smtpServer Two opts available: open and office
	 */
	public MailClient (String mailSenderUrl, String smtpServer) {
		this.initialize(mailSenderUrl, smtpServer);
	}
	
	private void initialize(String mailSenderUrl, String smtpServer) {
		this.mailSenderUrl = mailSenderUrl;
		HttpHeaders headers = new HttpHeaders();
		if (smtpServer != null) {
			headers.add("smtpServer", smtpServer);
		}
	    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	    this.headers = headers;
	}
	
	private RestTemplate restTemplate;

	/**
	 * Send email using a template
	 * @param sentToEmail
	 * @param template
	 * @param variables
	 * @param subject
	 * @param images
	 * @param application application where the email is sent from
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables,
		String subject, Map<String, InputStream> images, String application) throws IOException {
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, null, null, null, null, null, null, application);
    }
	
	/**
	 * 
	 * @param to
	 * @param inputStream
	 * @param vars
	 * @param subject
	 * @param inlines
	 * @param attachments
	 * @param application
	 * @throws IOException 
	 */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables, String subject,
			Map<String, InputStream> images, Map<String, InputStream> attachments, String[] copyTo, String application) throws IOException {
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, attachments, null, copyTo, null, null, null, application);
	}
	
	/**
	 *
	 * @param sentToEmail
	 * @param template
	 * @param variables key is the reference of the value in the template
	 * @param subject
	 * @param images key is the reference of the image in the template
	 * @param from
	 * @param application application where the email is sent from
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables,
		String subject, Map<String, InputStream> images, String from, String application) throws IOException {
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, null, from, null, null, null, null, application);
    }

	/**
	 *
	 * @param sentToEmail
	 * @param template
	 * @param variables key is the reference of the value in the template
	 * @param subject
	 * @param images key is the reference of the image in the template
	 * @param from
	 * @param copyTo
	 * @param application application where the email is sent from
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables,
		String subject, Map<String, InputStream> images, String from, String[] copyTo, String application) throws IOException {
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, null, from, copyTo, null, null, null, application);
    }

	/**
	 *
	 * @param sentToEmail
	 * @param template
	 * @param variables key is the reference of the value in the template
	 * @param subject
	 * @param images key is the reference of the image in the template
	 * @param from
	 * @param copyTo
	 * @param cco
	 * @param templateIS
	 * @param imagesIS
	 * @param application application where the email is sent from
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, InputStream template, Map<String, ?> variables, String subject,
		Map<String, InputStream> images, Map<String, InputStream> attachments, String from, String[] copyTo, String [] cco, InputStream templateIS, Map<String, InputStream> imagesIS, String application) throws IOException {
        HashMap<String, String> inlines = new HashMap<>();
        HashMap<String, String> encodedAttachments = new HashMap<>();
        if (images != null) {
        	for (Map.Entry<String, InputStream> entry : images.entrySet()) {
            	byte[] readFileToByteArray = new byte[] {};
    			readFileToByteArray = IOUtils.toByteArray(entry.getValue());
            	String encodedImage = Base64.getEncoder().encodeToString(readFileToByteArray);
            	inlines.put(entry.getKey(), encodedImage);
            }
        } else {
        	if (imagesIS != null) {
        		for (Map.Entry<String, InputStream> entry : imagesIS.entrySet()) {
                	byte[] readFileToByteArray = new byte[] {};
        			readFileToByteArray = IOUtils.toByteArray(entry.getValue());
                	String encodedImage = Base64.getEncoder().encodeToString(readFileToByteArray);
                	inlines.put(entry.getKey(), encodedImage);
                }
        	}
        }
        if(attachments != null) {
    		for (Map.Entry<String, InputStream> entry : attachments.entrySet()) {
            	byte[] readFileToByteArray = new byte[] {};
    			readFileToByteArray = IOUtils.toByteArray(entry.getValue());
            	String encodedImage = Base64.getEncoder().encodeToString(readFileToByteArray);
            	encodedAttachments.put(entry.getKey(), encodedImage);
            }        	
        }
		String templateBytesString = "";
		if (template != null) {
			templateBytesString = new String(IOUtils.toByteArray(template), "UTF-8");
		} else {
			templateBytesString = new String(IOUtils.toByteArray(templateIS), "UTF-8");
		}
		String inlinesString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(inlines);
		String attachmentsString = new ObjectMapper().configure(Feature.IGNORE_UNDEFINED, true).writeValueAsString(encodedAttachments);
        String templateVariables = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(variables);
        String sentToEmailString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(sentToEmail);
        String copyToString = "";
        if (copyTo != null)
        	copyToString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(copyTo);
        String ccoString = "";
        if (cco != null)
        	ccoString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(cco);
        MultiValueMap<String, Object>  requestBody= new LinkedMultiValueMap<>();
        requestBody.add("sentToEmail", sentToEmailString);
        requestBody.add("inlines", inlinesString);
        requestBody.add("attachments", attachmentsString);
        requestBody.add("subject", subject);
        requestBody.add("template", templateBytesString);
        requestBody.add("templateVariables", templateVariables);
        requestBody.add("application", application);
        if (from != null)
        	requestBody.add("from", from);
        if (!copyToString.isEmpty())
        	requestBody.add("copyTo", copyToString);
        if (!ccoString.isEmpty())
        	requestBody.add("cco", ccoString);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(requestBody, getHeaders());

        return getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mail", request, Object.class);
    }

	/**
	 *
	 * @param sentToEmail
	 * @param content
	 * @param subject
	 * @param images key is the reference of the image in the template
	 * @param from
	 * @param copyTo
	 * @param cco
	 * @param application application where the email is sent from
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<?> sendEmailWithContent(String[] sentToEmail,String content , String subject, Map<String, InputStream> images,
		String from, String[] copyTo,String [] cco, String application) throws IOException {
		HashMap<String, String> inlines = new HashMap<>();
		if (images != null) {
			for (Map.Entry<String, InputStream> entry : images.entrySet()) {
				byte[] readFileToByteArray = new byte[] {};
				readFileToByteArray = IOUtils.toByteArray(entry.getValue());
				String encodedImage = Base64.getEncoder().encodeToString(readFileToByteArray);
				inlines.put(entry.getKey(), encodedImage);
			}
		}
		String inlinesString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(inlines);
			HashMap<String, Object>  requestBody= new HashMap<>();
		requestBody.put("sentToEmail", sentToEmail);
		requestBody.put("subject", subject);
		requestBody.put("content", content);
		requestBody.put("inlines", inlinesString);
		requestBody.put("copyTo", copyTo);
		requestBody.put("cco", cco);
		requestBody.put("application", application);
		HttpHeaders httpHeaders = getHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(requestBody, httpHeaders);

		return getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mailWithContent", request, Object.class);
	}
	
	/**
	 *
	 * @param message
	 * @param appDesc a descriptor for the app calling this method. Will be used to form description in feedback template. e.g: Customer Center
	 * @param appName Will be used to match the logo of the app in mail-sender. e.g customercenter.
	 * @param userEmail
	 * @param userCompany
	 * @param userName
	 * @param subject
	 * @param application application where the email is sent from
	 * @return
	 * @throws JsonProcessingException
	 */
    public ResponseEntity<?> sendFeedback(String message, String appDesc, String appName, String userEmail, String userCompany,
		String userName, String subject, String application) throws JsonProcessingException {
        HashMap<String, String> variables = new HashMap<>();
        variables.put("message", message);
        variables.put("appDesc", appDesc);
        variables.put("userEmail", userEmail);
        variables.put("userCompany", userCompany);
        variables.put("userName", userName);
        String templateVariables = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(variables);
        MultiValueMap<String, String>  requestBody= new LinkedMultiValueMap<>();
        requestBody.add("appName", appName);
        requestBody.add("subject", subject);
        requestBody.add("templateVariables", templateVariables);
        requestBody.add("application", application);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, getHeaders());

        return getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mail/feedback", request, Object.class);
    }

	/**
	 * Send a recovery email to the user
	 * @param userEmail
	 * @param hash
	 * @param appUrl
	 * @param subject
	 * @param application application where the email is sent from
	 * @return
	 * @throws JsonProcessingException
	 */
    public ResponseEntity<?> sendRecovery(String userEmail, String hash, String appUrl, String subject, String application) throws JsonProcessingException {
    	HashMap<String, String> variables = new HashMap<>();
    	variables.put("username", userEmail);
    	variables.put("hash", hash);
    	variables.put("appUrl", appUrl);
    	String templateVariables = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(variables);
    	MultiValueMap<String, String> requestBody= new LinkedMultiValueMap<>();
    	requestBody.add("username", userEmail);
        requestBody.add("subject", subject);
        requestBody.add("templateVariables", templateVariables);
        requestBody.add("application", application);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, getHeaders());

        return getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mail/recovery", request, Object.class);
    }
	
	private RestTemplate getRestTemplate() {
		return restTemplate == null ? restTemplate = new RestTemplate() : restTemplate;
	}
	private HttpHeaders getHeaders() {
	     return this.headers;
	}

	/**
	 * Get the mail notifications send for the user and application
	 * @param email
	 * @param application application where the email is sent from
	 * @return
	 */
	public ResponseEntity<?> getMailNotifications(String email, String application) {
		UriComponentsBuilder builder = UriComponentsBuilder
			.fromUriString(mailSenderUrl + "mail-sender/mail/notifications")
			.queryParam("email", email)
			.queryParam("application", application);

		return getRestTemplate().getForEntity(builder.toUriString(), Object.class);
	}

}
